# Project Haystack Example  Data Models 

This is a simple site to demonstrate [Project Haystack](https://project-haystack.org/) data models.

## Getting Started

Models in Haystack JSON format are loaded by a crude script and are rendered by [vis.js](http://visjs.org/). To make a model for visulisation, either save a json readByFilter response from a Haystack Server, or build the JSON by hand.


##TODO

1) Add some simple examples, e.g. baisc Haystack structure - site, equip, point, weather
2) Add example of different parts of a builiding. e.g. HVAC, Electrical switchbaords


## Contributing

Feel free to add or improve the example by submitting pull requests.


## License

This project is licensed under the MIT License


